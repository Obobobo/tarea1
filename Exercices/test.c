#include <stdio.h> 
#include <string.h> 
#include <stdlib.h>
 
int main(int argc, char **argv) 
{ 
	// Lee el nombre del archivo 
	FILE* fp = fopen(argv[1], "r");

	float mean = 0;

	if (!fp) 
		printf("No se puede abrir su archivo\n"); 

	else { 
		// Creamos un arreglo con 1023 de memoria para recibir las lineas del archivo
		char buffer[1024];

		int fila = 0; 
		int columna = 0; 

		while (fgets(buffer, 1024, fp)) {

			columna = 0; 
			fila++; 

			// Omitimos la primera fila para no imprimir las categorias
			if (fila == 1) 
				continue; 

			// Separamos los datos del archivo por medio de comas 
			char* value = strtok(buffer, ","); 

			while (value) { 

				if (columna == 0) { 
					printf("Nombre: ");
					
				} 

				if (columna == 1) { 
					printf("\nNota 1: ");

					//Transformamos el string apuntado por "value" a un numero float y lo asignamos a una variable
                    float x = strtof(value,NULL);
					mean += x;
				} 

				if (columna == 2) { 
					printf("\nNota 2: ");
                    float x = strtof(value,NULL);
					mean += x;
				} 

				if (columna == 3) { 
					printf("\nNota 3: ");
					float x = strtof(value,NULL);
					mean += x; 
				} 

				if (columna == 4) { 
					printf("\nNota 4: ");
					float x = strtof(value,NULL);
					mean += x; 
				} 

				if (columna == 5) { 
					printf("\nNota 5: ");
					float x = strtof(value,NULL);
					mean += x; 
				} 

				if (columna == 6) { 
					printf("\nNota 6: ");
					float x = strtof(value,NULL);
					mean += x; 
				} 

				printf("%s", value); 
				value = strtok(NULL, ","); 
				columna++;
			} 

			//Obtenemos el promedio
			mean = mean/6;

			printf("El promedio final es: %.1f", mean);
			printf("\n");
			printf("\n");

			//Reiniciamos el valor para el proximo alumno
			mean = 0;

		} 

		fclose(fp); 
	} 
	return 0; 
} 
